class Code
  attr_reader :pegs

  def initialize(pegs_array)
    @pegs = pegs_array
  end

  PEGS =
      {
      red: ["R", "r"],
      yellow: ["Y", "y"],
      blue: ["B", "b"],
      green: ["G", "g"],
      orange: ["O", "o"],
      purple: ["P", "p"]
      }

  def self.random
    new_array = []
    colors_array = ["b", "r", "g", "y", "o", "p"]
    until new_array.length == 4
      new_array << colors_array.shuffle[0]
    end
    Code.new(new_array)
  end

  def self.parse(str)
    colors = str.downcase.chars
    colors.each do |color|
      if PEGS.values.flatten.include?(color)
        next
      else
        raise "not a color"
      end
    end
    Code.new(colors)
  end

  def exact_matches(guesses)
    idx = 0
    matches = 0
    while idx < 4
      if guesses[idx] == self[idx]
        matches += 1
      end
      idx += 1
    end
    matches
  end

  def near_matches(guesses)
    total = 0
    guesses_dup = guesses.pegs.dup
    pegs_dup = self.pegs.dup
    pegs_dup.each do |x|
      if guesses_dup.count(x) > pegs_dup.count(x)
        total += pegs_dup.count(x)
      elsif pegs_dup.count(x) > guesses_dup.count(x)
        total += guesses_dup.count(x)
      elsif guesses_dup.count(x) == pegs_dup.count(x)
        total += guesses_dup.count(x)
      end
      guesses_dup.delete(x)

    end
    total - exact_matches(guesses)
  end


  def [](index)
    @pegs[index]
  end

  def ==(code)
    if code.class != Code
      return false
    else
      self.pegs.join.downcase == code.pegs.join.downcase
    end
  end

end

class Game
  attr_reader :secret_code

  def initialize(code = Code.random)
    @secret_code = code
  end

  def play
    turns = 0
    until turns == 10
      guess = self.get_guess
      self.display_matches(guess)
      turns += 1
      if @secret_code.exact_matches(guess) == 4
        puts "You won in #{turns} turns!"
        break
      else
        puts "You have #{(10) - turns} turns left"
      end
    end
  end

  def get_guess
    puts "please guess 4 colors(choices are: rgbyop) in specific order(i.e. RGBY or rgby)"
    response = $stdin.gets.chomp
    Code.parse(response)
  end

  def display_matches(guess)
    puts "You had #{@secret_code.near_matches(guess)} near matches and #{@secret_code.exact_matches(guess)} exact matches"
  end

end

if __FILE__ == $PROGRAM_NAME
  Game.new.play
end
